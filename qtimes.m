function [ r ] = qtimes( q, p )
%QUATTIMES Summary of this function goes here
%   Detailed explanation goes here
qv = q(2:4,1);
pv = p(2:4,1);
r = [q(1)*p(1) - qv'*pv; q(1)*pv + p(1)*qv + cross(qv, pv)];

end

