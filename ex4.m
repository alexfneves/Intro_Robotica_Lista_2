syms s1 s23 s4 s5 s6 c1 c23 c4 c5 c6 real;

x = [1 0 0];
y = [0 1 0];
z = [0 0 1];

q01 = [c1 s1*z]';
q13 = [c23 s23*x]';
q34 = [c4 s4*y]';
q45 = [c5 s5*x]';
q56 = [c6 s6*y]';

q0e = qtimes(q01, qtimes(q13, qtimes(q34, qtimes(q45,q56))));

q0e_s = q0e(1,1);
q0e_v = q0e(2:4,1);
R = (2*q0e_s^2 - 1)*eye(3) + 2*(q0e_v*q0e_v' + q0e_s*hat(q0e_v))