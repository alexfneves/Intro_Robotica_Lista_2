clear;

syms alpha theta1 theta2 theta3 l1 l2 l3 real;
x = [1 0 0]';
y = [0 1 0]';
z = [0 0 1]';

%expm(hat(alpha*z))

% T = [eye(3) zeros(3,1); zeros(1,3) 1];

R01 = rotz(theta1);
R12 = rotx(-theta2);
hat_h3 = hat(cos(alpha)*z + sin(alpha)*y);
R23 = eye(3) + sin(theta3)*hat_h3 + (1 - cos(theta3))*hat_h3^2;
R23 = simplify(R23)

%T03 = T01*T02*T03*T04