clear;

a1 = 0.5;
a2 = 0.3;
a3 = 0.2;

p0e = [];
for t1 = -pi/3:0.05:pi/3
    for t2 = -2*pi/3:0.05:2*pi/3
        for t3 = -pi/2:0.05:pi/2
            t12 = t1 + t2;
            t123 = t12 + t3;
            p = a1*[cos(t1); sin(t1)] + a2*[cos(t12); sin(t12)] + a3*[cos(t123); sin(t123)];
            p0e = [p0e p];
        end
    end
end

plot(p0e(1,:)', p0e(2,:)','.')
export_fig('latex/figs/ex5_esp_trab', '-pdf', '-painters', '-transparent');